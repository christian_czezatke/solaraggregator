#!/usr/bin/python
"""SolarAggregator component that uploads solar and smart meter data to the
   Bidgely Power Monitoring service.

   (C) 2015, Christian Czezatke"""

import time
import xml.etree.cElementTree as ET
import Pyro4
import requests
from settings import settings
#import utils
import mylogger

MYSETTINGS = settings['uploader']
BIDGELY_URL = 'https://api.bidgely.com/v1/users/' + MYSETTINGS['bidgely_key'] + \
              '/homes/1/gateways/1/upload'


class BidgelyUploader(object):
    """Object to do periodic uploads to Bidgely by polling the homedb database service
    for solar and meter info."""

    def __init__(self, url):
        self._url = url

    def make_xml(self, polldata):
        """Convert the data polled from the database into an XML message that can be
        consumed by Bidgely. Returns the XML string."""

        root = ET.Element('upload', version='1.0')
        meters = ET.SubElement(root, 'meters')

        ## PG&E Meter
        metermacid = settings['rainforest']['metermacid']
        pge = ET.SubElement(meters, 'meter', id=metermacid, model='SEP/1.x',
                            type="2", description="Zigbee Electricity Meter")
        streams = ET.SubElement(pge, 'streams')

        delivered = ET.SubElement(streams, 'stream', id='CurrentSummationDelivered',
                                  unit='kWh', description='PG&E Import')
        export = ET.SubElement(streams, 'stream', id='CurrentSummationReceived',
                               unit='kWh', description='PG&E Export')

        for elem in polldata['pge']:
            ET.SubElement(delivered, 'data', time='{0}'.format(elem[0]),
                          value='{0:.3f}'.format(float(elem[1])/1000))
            ET.SubElement(export, 'data', time='{0}'.format(elem[0]),
                          value='{0:.3f}'.format(float(elem[2])/1000))

        stream = ET.SubElement(streams, 'stream', id='InstantaneousDemand',
                               unit='kW', description='Real-Time Demand')
        for elem in polldata['demand']:
            ET.SubElement(stream, 'data', time='{0}'.format(elem[0]),
                          value='{0:.3f}'.format(float(elem[1])/1000))

        ## Envoy Aggregator
        solar = ET.SubElement(meters, 'meter', id=settings['envoy']['serialno'],
                              model='ENVOY', type='1', description='Enphase Envoy')
        streams = ET.SubElement(solar, 'streams')
        output = ET.SubElement(streams, 'stream', id='CurrentSummationDelivered',
                               unit='kWh', description='Solar Generation')
        #solNow = ET.SubElement(streams, 'stream', id='InstantaneousDemand',
        #                       unit='kW', description='Solar Current')
        for elem in polldata['solar']:
            ET.SubElement(output, 'data', time='{0}'.format(elem[0]),
                          value='{0:.3f}'.format(float(elem[1])/1000))
           #ET.SubElement(solNow, 'data', time='{0}'.format(elem._timestamp),
           #              value='{0:.3f}'.format(float(elem._kwNow)))

        xmlstr = ET.tostring(root, encoding='utf8', method='xml')
        return xmlstr

    def update(self, xmlstr):
        """Post XML data to Bidgely."""

        requests.post(self._url, data=xmlstr)


def run():
    """Main function. Start querying and upload to bidgely."""

    logger, handler = mylogger.get_logger(MYSETTINGS['logger'])
    mylogger.set_logger(logger)

    uploader = BidgelyUploader(BIDGELY_URL)

    svc_name = "PYRO:{0}@{1}:{2}".format(settings['homedb']['service'],
                                         settings['homedb']['servicehost'],
                                         settings['homedb']['serviceport'])

    while True:
        try:
            now = time.time()
            dbase = Pyro4.Proxy(svc_name)
            polldata = dbase.get_latest_values(3, ['demand', 'pge', 'solar'])
            xmlstr = uploader.make_xml(polldata)
            #print rainforest.PrettyXML(xmlstr)
            uploader.update(xmlstr)

            delta = now + 10 - time.time()
            if delta < 0:
                print "Cannot keep up with posting"
                delta = 1
            time.sleep(delta)
        except Exception, exc:
            print "Error: {0:}".format(exc)


if __name__ == "__main__":
    run()
