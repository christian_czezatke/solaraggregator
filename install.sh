#!/bin/bash

BINDIR=/opt/solar
DATADIR=/var/solar
LOGDIR=$DATADIR/logs
HTTPDIR=/var/www
APILOGDIR=$DATADIR/apilogs

WWWUSER=www-data
WWWGROUP=www-data

echo "Setting up directories"

mkdir -p $BINDIR
mkdir -p $DATADIR
mkdir -p $LOGDIR
mkdir -p $APILOGDIR

# Allow apache plugin to write to api logdir.
chown $WWWUSER:$WWWGROUP $APILOGDIR

echo "Installing Web Components"

cp ifng/sol2.html ifng/graphing.js $HTTPDIR
chmod a+r $HTTPDIR/sol2.html
chmod a+r $HTTPDIR/graphing.js

mkdir -p $HTTPDIR/api
cp sdata.wsgi $HTTPDIR/api
chmod a+r $HTTPDIR/api/sdata.wsgi

echo "Installing scripts"
cp *py $BINDIR

echo "Registering service"
cp solar /etc/init.d
insserv -r solar
update-rc.d solar defaults

echo <<EOF
Put the following in /etc/apache/sites-available/default:

        WSGIScriptAlias /api /var/www/api
        <Directory /var/www/api>
                AddHandler wsgi-script .wsgi
                Order allow,deny
                Allow from all
        </Directory>

And then do a "service apache2 restart".
EOF
