#!/usr/bin/python
"""Miscellaneous utilities used by different parts of SolarAggregator.
   (C) 2015, Christian Czezatke"""

from mylogger import info, warn
import Pyro4
import time


def wait_for_pyro(svclist):
    """Wait for the pyro name service to become available and make sure
    that all the services listed in svclist are known and available."""

    info("Waiting for PYRO name server.")
    found = False
    while not found:
        try:
            nsvc = Pyro4.naming.locateNS()
            info("Found the folloing services: {0:}".format(nsvc.list()))
            found = True
        except Exception:
            time.sleep(2)
    info("Found PYRO name server.")
    info("Waiting for Switch Service.")
    found = False
    current = ''
    while not found:
        try:
            cnt = 0
            for svc in svclist:
                current = svc
                proxy = Pyro4.Proxy(svc)
                proxy.ping()
                cnt += 1
                info("Found Service {0:}".format(svc))
            found = (cnt == len(svclist))
        except Exception, exc:
            warn("Got exception {0:} while looking for {1:}".format(exc, current))
            time.sleep(2)
    info("All required services found.")


def pretty_xml(instr, incr=3):
    """Exported helper function to convert an XML string into something that is
    neatly formatted, with indentation. instr is the XML string and incr is the
    indentation amount. Returns a neatly formatted XML string, which is handy
    for debugging."""

    outstr = ""
    lines = instr.split('>')
    level = 0
    for line in lines[1:]:
        if line == '':
            continue
        post = 0
        line += '>'
        if line[0:2] == '</':
            # End of a Tag
            level -= 1
        elif line[-2:] != '/>':
            post = 1
        line = ' ' * level * incr + line
        outstr += line + '\n'
        level += post
    return lines[0] +'>' + outstr


def _test():
    """Unit test this module."""

    import mylogger
    logger, handler = mylogger.get_logger('foo.log')
    mylogger.set_logger(logger)
    wait_for_pyro([])


if __name__ == '__main__':
    _test()
