"""Module to read data from a smart meter via a Rainforest Eagle Zigbee gateway.
   (C) 2015, Christian Czezatke"""

import xml.etree.ElementTree as ET
import time
import socket
from mylogger import error, info

# Unix epoch at which the rainforest eagle starts its timestamps.
RAINFOREST_EPOCH = 946684809

# Address to be set by init.
RAINFOREST_ADDRESS = None

def _get_timestamp(node):
    """Extract a "TimeStamp" node from an XML tree and convert it from
    Rainforest's epoch to a proper Unix timestamp."""

    return int(node.find('TimeStamp').text, 16) + RAINFOREST_EPOCH


def _rainforest_timestamp(epoch):
    """Convert a unix timestamp to a rainforest timestamp."""

    return epoch - RAINFOREST_EPOCH


def _get_value(node, key):
    """Extract the meter value for an XML entry identified by key from
    a subtree identfied by node and return it. The Eagle returns a
    hex number that represents a 32 bit signed integer, but the
    number also needs to be shifted and divided according to the
    message returned.

    Returns a floatingn point number corresponding to the correted value."""

    value = int(node.find(key).text, 16)
    if value > 0x7FFFFFFF:
        value -= 0x100000000
    multiplier = int(node.find('Multiplier').text, 16)
    divisor = int(node.find('Divisor').text, 16)
    if divisor == 0:
        divisor = 1
    if multiplier == 0:
        multiplier = 1

    return float(value) * float(multiplier) / float(divisor)


def _send_command(command, **kwargs):
    """Send a command to the Rainforest Eagle using its local TCP socket API.
    Note that the Eagle is very picky about whitespaces... :/"""

    txt = "<LocalCommand>\n"
    txt += "<Name>" + command + "</Name>\n"
    for key, value in kwargs.items():
        txt += "<" + key + ">" + value + "</" + key + ">\n"
    txt += "</LocalCommand>\n"

    soc = socket.create_connection((RAINFOREST_ADDRESS, 5002), 10)
    soc.sendall(txt)
    #print "HD SENT " + txt

    data = ''
    while True:
        try:
            buf = soc.recv(4096)
        except Exception, exc:
            error("Problem receiving data from the Rainforest Eagle: {0:}"\
                  .format(exc))
            break
        if not buf:
            break
        data = data + buf
    #print "GOT: {0:}".format(data)
    return data



class InstantaneousDemand(object):
    """
    Represents an "instantaneous demand" reading of the smart meter.

    <?xml version="1.0"?>
    <rainforest macId="0xd8d5b900253b" version="1.1" timestamp="1427048815s">
    <InstantaneousDemand>
    <DeviceMacId>0xd8d5b90000003e00</DeviceMacId>
    <MeterMacId>0x0013500101504e59</MeterMacId>
    <TimeStamp>0x1ca1c5e8</TimeStamp>
    <Demand>0xffffff4a</Demand>
    <Multiplier>0x00000001</Multiplier>
    <Divisor>0x000003e8</Divisor>
    <DigitsRight>0x03</DigitsRight>
    <DigitsLeft>0x0f</DigitsLeft>
    <SuppressLeadingZero>Y</SuppressLeadingZero>
    <Port>/dev/ttySP0</Port>
    </InstantaneousDemand>
    </rainforest>
    """

    def __init__(self, tree):
        self.timestamp = _get_timestamp(tree)
        self.demand = _get_value(tree, 'Demand')
        self.meter_id = tree.find('MeterMacId').text[2:]


class NetworkInfo(object):
    """
    Information about the Zigbee link that the Rainforest Eagle has established
    to your smart meter.

    <?xml version="1.0"?>
    <rainforest macId="0xd8d5b900253b" version="1.1" timestamp="1427068021s">
    <NetworkInfo>
    <DeviceMacId>0xd8d5b90000003e00</DeviceMacId>
    <CoordMacId>0x0013500101504e59</CoordMacId>
    <Status>Connected</Status>
    <Description>Successfully Joined</Description>
    <ExtPanId>0x0013500101504e59</ExtPanId>
    <Channel>20</Channel>
    <ShortAddr>0xfa82</ShortAddr>
    <LinkStrength>0x64</LinkStrength>
    <Port>/dev/ttySP0</Port>
    </NetworkInfo>
    </rainforest>
    """

    def __init__(self, tree):
        self.meter_id = tree.find('CoordMacId').text[2:]
        connstr = tree.find('Status').text
        self.connected = connstr.lower() == 'connected'
        self.signal_strength = int(tree.find('LinkStrength').text, 16)
        self.timestamp = int(time.time())
        self.device_mac_id = tree.find('DeviceMacId').text[2:]


class PriceCluster(object):
    """
    Pricing information as obtained from the smart meter.

    <?xml version="1.0"?>
    <rainforest macId="0xd8d5b900253b" version="1.1" timestamp="1427069113s">
    <PriceCluster>
    <DeviceMacId>0xd8d5b90000003e00</DeviceMacId>
    <MeterMacId>0x0013500101504e59</MeterMacId>
    <TimeStamp>0x1c9fd249</TimeStamp>
    <Price>0x00000010</Price>
    <Currency>0x0348</Currency>
    <TrailingDigits>0x02</TrailingDigits>
    <Tier>0x01</Tier>
    <StartTime>0xffffffff</StartTime>
    <Duration>0xffff</Duration>
    <RateLabel>Set by User</RateLabel>
    <Port>/dev/ttySP0</Port>
    </PriceCluster>
    """

    def __init__(self, tree):
        self.timestamp = _get_timestamp(tree)
        self.meter_idd = tree.find('MeterMacId').text[2:]
        price = int(tree.find('Price').text, 16)
        divider = int(tree.find('TrailingDigits').text, 16)
        self.price = (float(price) / float(10 * divider))


class CurrentSummation(object):
    """
    Information about a history interval as stored in the smart meter.
    Typically, a PGE smart meter maintains one record like this for every
    5 minute interval.

    <CurrentSummationDelivered>
    <DeviceMacId>0xFFFFFFFFFFFFFFFF</DeviceMacId>
    <MeterMacId>0xFFFFFFFFFFFFFFFF</MeterMacId>
    <TimeStamp>0xFFFFFFFF</TimeStamp>
    <SummationDelivered>0xFFFFFFFF</SummationDelivered>
    <SummationReceived>0xFFFFFFFF</SummationReceived>
    <Multiplier>0xFFFFFFFF</Multiplier>
    <Divisor>0xFFFFFFFF</Divisor>
    <DigitsRight>0xFF</DigitsRight>
    <DigitsLeft>0xFF</DigitsLeft>
    <SuppressLeadingZero>{enumeration}</SuppressLeadingZero>
    </CurrentSummationDelivered>
    """

    def __init__(self, tree):
        self.timestamp = _get_timestamp(tree)
        self.meter_id = tree.find('MeterMacId').text[2:]
        self.delivered = _get_value(tree, 'SummationDelivered')
        self.received = _get_value(tree, 'SummationReceived')


class HistoryData(object):
    """Get information about a sequence of measurement intervals. See
    CurrentSummation."""

    def __init__(self, tree):
        # A list of current summation...
        self.data = []
        for elem in tree:
            self.data.append(CurrentSummation(elem))


def init(address):
    """Initialize the rainforest module. Set its IP address."""

    global RAINFOREST_ADDRESS
    RAINFOREST_ADDRESS = address


def get_history_data(device_mac_id, starttime=RAINFOREST_EPOCH,
                     endtime=None, frequency=None):
    """Get historic data from a smart meter. Specify a start and end time
    and a frequency (in seconds) and the meter will return a lists of
    intervals, with timestamp, import and export numbers."""

    args = {}
    args['MacId'] = '0x{0:}'.format(device_mac_id)
    args['StartTime'] = '0x{0:x}'.format(_rainforest_timestamp(starttime))
    if endtime:
        args['EndTime'] = '0x{0:x}'.format(_rainforest_timestamp(endtime))
    if frequency:
        args['Frequency'] = '0x{0:x}'.format(frequency)

    result = _send_command('get_history_data', **args)
    info("Eagle returned: {0:}".format(result))
    tree = ET.fromstring(result)
    return HistoryData(tree)


def get_instantaneous_demand(device_mac_id):
    """Get the current demand as reported by the smart meter."""

    result = _send_command('get_instantaneous_demand',
                           MacId='0x{0:}'.format(device_mac_id))
    tree = ET.fromstring(result)
    return InstantaneousDemand(tree)
