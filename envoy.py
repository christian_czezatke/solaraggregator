"""Wrapper function to poll data from an Enphase Enovy gateway
using its local JSON interface.

   (C) 2015, Christian Czezatke"""

import requests
import time
from settings import settings

MYSETTINGS = settings['envoy']

class EnvoyData(object):
    """Envoy data as polled from its local JSON interface."""
    def __init__(self, json):
        self.kw_now = float(json['wattsNow']) / 1000
        self.watthours_seven_days = json['wattHoursSevenDays']
        self.watthours_lifetime = json['wattHoursLifetime']
        self.meter_id = MYSETTINGS['serialno']
        self.timestamp = int(time.time())


def get_current_data():
    """Query the Enphase Envoy, construct an EnvoyData object and return the
    current production data. Timestamp it with the current timestamp, since
    the Envoy does not do this."""

    try:
        url = 'http://' + MYSETTINGS['address'] + '/api/v1/production'
        req = requests.get(url)
        return EnvoyData(req.json())
    except Exception:
        return None
