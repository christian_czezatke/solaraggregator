#!/usr/bin/python
""" Module providing the database functionality for SolarTracker
   (C) 2015, Christian Czezatke"""

import mylogger
from mylogger import error, warn, info
import time
import sqlite3 as db
import threading
import Pyro4
import rainforest
import envoy
from settings import settings

MYSETTINGS = settings['homedb']

class WorkItem(object):
    """A WorkItem is a database request to be processed by the database
    worker thread. It includes a SQL statement when passed to the worker
    thread, and a series of result tuples or an exception."""

    # pylint: disable=too-few-public-methods

    def __init__(self, stmt, args):
        self.stmt = stmt
        self.args = args
        self.event = threading.Event()
        self.result = None
        self.error = None

    def run(self):
        """ Enqueue the WorkItem for processing and block until processing
        by the database thread is complete."""

        while not self.event.is_set():
            self.event.wait()
        if self.error is not None:
            raise Exception(self.error)
        else:
            return self.result


class HomeDB(object):
    """Class encapsulating the database storing solar and meter readings.
    It contains a worker thread that processes all sqlite requests in serial."""

    def _assure_tables(self):
        """Helper function for database worker thread that will create the
        requred database tables if they don't already exist. Also obtains
        a database cursor and returns it as result."""

        self._conn = db.connect(MYSETTINGS['dbname'])
        cursor = self._conn.cursor()

        cursor.execute(
            """create table if not exists PGE
               (timestamp int PRIMARY KEY, total_in int, total_out int)"""
        )

        cursor.execute(
            """create table if not exists SOLAR
            (timestamp int PRIMARY KEY, total_in int)"""
        )

        cursor.execute(
            """create table if not exists DEMAND
            (timestamp int PRIMARY KEY, demand int)"""
        )
        return cursor


    def _dbworker(self):
        """Database worker thread. Sleeps on self._work_indicator and wakes up and
        processes incoming requests."""

        info("Worker thread is up.")
        cursor = self._assure_tables()

        while True:
            while not self._work_indicator.is_set():
                self._work_indicator.wait()

            with self._lock:
                self._work_indicator.clear()
                for workitem in self._queue:
                    # See if we were signalled to stop...
                    if workitem.stmt == 'STOP':
                        workitem.event.set()
                        break

                    try:
                        cursor.execute(workitem.stmt, workitem.args)
                        workitem.result = cursor.fetchall()
                        self._conn.commit()
                    except Exception, exc:
                        warn("SQL ERROR in worker thread: {0:} for query {1:} ({2:})"\
                             .format(exc, workitem.stmt, workitem.args))
                        workitem.error = exc
                    workitem.event.set()
                self._queue = []


    def __init__(self):
        self._conn = None
        self._queue = []
        self._lock = threading.Lock()
        self._work_indicator = threading.Event()

        self._worker = threading.Thread(target=self._dbworker)
        self._worker.start()


    def _post_item(self, workitem):
        """Enqueue a WorkItem instance in the list of requests and wake up the
        worker thread."""

        with self._lock:
            self._queue.append(workitem)
            self._work_indicator.set()
        return workitem.run()


    def insert_pge(self, timestamp, total_in, total_out):
        """Insert PGE aggregate value for timestamp "timestamp". A PGE meter maitains
        one count for totak KWH in and another count for total KWH out (export)."""

        try:
            info("PGE INSERT: {0:} {1:}".format("insert into PGE values(?, ?, ?)",
                                                (timestamp, total_in, total_out)))
            self._post_item(WorkItem("insert into PGE values(?, ?, ?)",
                                     (timestamp, total_in, total_out)))
        except db.IntegrityError:
            # Value for timestamp already in database...
            info("PGE value for {0:}  already in database.".format(timestamp))


    def insert_demand(self, timestamp, demand):
        """Insert the current demand value into the demands database. A PGE meter
        updates the current demand about once every ten seconds."""

        try:
            self._post_item(WorkItem("insert into DEMAND values(?, ?)",
                                     (timestamp, int(demand*1000))))
        except db.IntegrityError:
            info("Demand value for {0:} already in database.".format(timestamp))


    def insert_solar(self, timestamp, total_in):
        """Insert the current "lifetime production" value into the solar database.
        The Enphase Envoy updates this every 5 minutes, but we have to poll it and
        it does not assign a timestamp to it by itself..."""

        try:
            info("SOLAR INSERT: {0:} {1:}".format("insert into SOLAR values(?, ?)",
                                                  (timestamp, total_in)))
            self._post_item(WorkItem("insert into SOLAR values(?, ?)",
                                     (timestamp, total_in)))
        except db.IntegrityError:
            info("Solar value for {0:} already in database.".format(timestamp))


    def get_latest_values(self, count, what):
        """Get the latest values for demand/pge/solar. The info returned is the
        same as described in get_values_for."""

        result = {}
        if 'demand' in what:
            result['demand'] = \
                self._post_item(WorkItem("select timestamp, demand from DEMAND "
                                         "order by timestamp DESC LIMIT ?", (count,)))

        if 'pge' in what:
            result['pge'] = \
                self._post_item(WorkItem("select timestamp, total_in, total_out from PGE "
                                         "order by timestamp DESC LIMIT ?", (count,)))

        if 'solar' in what:
            result['solar'] = \
                self._post_item(WorkItem("select timestamp, total_in from SOLAR "
                                         "order by timestamp DESC LIMIT ?", (count,)))

        return result


    def get_value_for(self, timestamp, what):
        """ Query values for a given timestamp:
            timestamp: Time for which to query (unix timestamp).
            what : list of demand/pge/solar.
        """

        tables = {
            'demand':['DEMAND', 'timestamp, demand'],
            'pge':['PGE', 'timestamp, total_in, total_out'],
            'solar':['SOLAR', 'timestamp, total_in']
        }

        querystr = """select {1:} from {0:} where timestamp in
            ((select min(timestamp) from {0:} where timestamp >= ?),
             (select max(timestamp) from {0:} where timestamp <= ?))
            order by timestamp ASC""".format(tables[what][0], tables[what][1])

        res = self._post_item(WorkItem(querystr, (timestamp, timestamp)))

        numfields = len(tables[what][1].split())
        if len(res) == 0:
            # we found nothing. Return -1 for all values
            result = tuple([-1 for x in range(numfields - 1)])
        elif len(res) == 1:
            # we found one item. Return that item in this case
            result = res[0][1:]
        elif len(res) == 2:
            # we found two items. -- Interpolate
            delta_t = res[1][0] - res[0][0]
            scale = float(timestamp - res[0][0]) / float(delta_t)
            result = tuple([res[0][i+1] + scale * (res[1][i+1]-res[0][i+1]) \
                            for i in range(numfields - 1)])
        else:
            raise Exception("Something is horribly wrong with the database")
        return result


    def get_interpolated_values_for(self, timelist, what):
        """Get values for a list of timestamps and for a list of properties. We
        return a dict, with the corresponding property as key, values are tuples
        of (timestamp, V1, [V2]). For 'demand' V1 is the current demand in Watts,
        for 'pge' V1 is the reading of the import counter, V2 is the reading of
        the export counter. for 'solar' V1 is the lifetime production reading.

        All values returned are in in Watts or Watthours.

        Also note that the timestamp returned might not match the timestamp requested.
        We return the latest database entry whose timestamp is not larger than the
        corresponding timestamp in the database."""

        result = {}

        for tval in timelist:
            result[tval] = {}
            for source in what:
                result[tval][source] = self.get_value_for(tval, source)
        return result


    def get_values_for(self, timelist, what):
        """Get values for a list of timestamps and for a list of properties. We
        return a dict, with the corresponding property as key, values are tuples
        of (timestamp, V1, [V2]). For 'demand' V1 is the current demand in Watts,
        for 'pge' V1 is the reading of the import counter, V2 is the reading of
        the export counter. for 'solar' V1 is the lifetime production reading.

        All values returned are in in Watts or Watthours.

        Also note that the timestamp returned might not match the timestamp requested.
        We return the latest database entry whose timestamp is not larger than the
        corresponding timestamp in the database."""

        tables = {
            'demand':['DEMAND', 'timestamp, demand'],
            'pge':['PGE', 'timestamp, total_in, total_out'],
            'solar':['SOLAR', 'timestamp, total_in']
        }

        result = {}

        for source in what:
            result[source] = []

            for tval in timelist:
                querystr = "select " + tables[source][1] + " from " + tables[source][0] + \
                           " where timestamp <= ? order by timestamp DESC LIMIT 1"

                res = self._post_item(WorkItem(querystr, (tval,)))
                if res == []:
                    # No items in this query
                    break

                res = res[0]

                if len(result[source]) == 0 or \
                   result[source][-1][0] != res[0]:
                    result[source].append(res)
        return result


    def get_range(self, start, end, what):
        """Get readings for a given list of properties within a range of time."""
        tables = {
            'demand':['DEMAND', 'timestamp, demand'],
            'pge':['PGE', 'timestamp, total_in, total_out'],
            'solar':['SOLAR', 'timestamp, total_in']
        }

        result = {}

        for source in what:
            querystr = "select " + tables[source][1] + " from " + tables[source][0] + \
                       " where timestamp >= ? and timestamp <= ? order by timestamp ASC"

            res = self._post_item(WorkItem(querystr, (start, end)))
            result[source] = res
        return result


    def debug_dump(self):
        """Dump the content of the entire database into the log file. Use with caution."""
        try:
            info("DEMAND")
            res = self._post_item(WorkItem("select * from DEMAND ORDER BY timestamp DESC", ()))
            for elem in res:
                info(elem)

            info("PGE")
            res = self._post_item(WorkItem("select * from PGE ORDER BY timestamp DESC", ()))
            for elem in res:
                info(elem)

            info("SOLAR")
            res = self._post_item(WorkItem("select * from SOLAR ORDER BY timestamp DESC", ()))
            for elem in res:
                info(elem)

        except Exception, exc:
            error("Dump problem: {0:}".format(exc))


class Poller(object):
    """Class to take care of polling both the smart meter and the solar generation
    data and putting it into a database. Launches a background thread that does the
    periodic polling."""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods

    def __init__(self, dbase, rainforest_address, meterMacId, pollWait, pgeMulti):
        self._meter_mac_id = meterMacId
        self._poll_wait = pollWait
        self._pge_multi = pgeMulti
        self._pge_counter = 0
        self._db = dbase
        self._previous_demand = -1000000000 # Safe unless you own a nuke plant...
        self._previous_solar = -1 #Always positive, hence safe...
        self._pge_timer = None
        self._solar_timer = None
        rainforest.init(rainforest_address)


    def run(self):
        """Start function to kick off periodic polling. Uses a threading.Timer object
        to (re)schedule periodic polling."""

        self._run_pge()
        self._run_solar()

    def _run_pge(self):
        """Spin up polling thread to pool the electric meter. Done in a separate
        thread since the meter some times takes a long time to reply."""

        now = time.time()
        try:
            self._poll_pge()
        except Exception, exc:
            error("PGE Poll failed: {0:}".format(exc))

        delta = now + self._poll_wait - time.time()
        if delta < 0:
            warn("Cannot keep up with PGE polling rate...")
            delta = 1
        self._pge_timer = threading.Timer(delta, self._run_pge)
        self._pge_timer.start()


    def _run_solar(self):
        """Spin up polling thread to poll solar production data."""

        now = time.time()
        try:
            self._poll_solar()
        except Exception, exc:
            error("Solar poll failed: {0:}".format(exc))

        delta = now + self._poll_wait - time.time()
        if delta < 0:
            warn("Cannot keep up with solar polling rate...")
            delta = 1
        self._solar_timer = threading.Timer(delta, self._run_solar)
        self._solar_timer.start()


    def _poll_pge(self):
        """Internal polling function. When invoked, polls the smart meter
        for latest consumption data and stores it into the database."""

        now = time.time()
        if (self._pge_counter % self._pge_multi) == 0:
            metered = rainforest.get_history_data(self._meter_mac_id,
                                                  int(now) - (10*self._poll_wait * self._pge_multi),
                                                  frequency=60)
            if len(metered.data) > 0:
                self._db.insert_pge(metered.data[-1].timestamp,
                                    metered.data[-1].delivered * 1000,
                                    metered.data[-1].received * 1000)
                self._pge_counter = 0
            else:
                # Log message and try again next time we wake up.
                warn("Could not get meter history data.")
                self._pge_counter = -1

        demand = rainforest.get_instantaneous_demand(self._meter_mac_id)
        if demand.demand != self._previous_demand:
            self._db.insert_demand(demand.timestamp, demand.demand)
            self._previous_demand = demand.demand

        self._pge_counter += 1


    def _poll_solar(self):
        """Internal polling function. When invoked, polls the solar array for
        latest production data and stores it into the database.

        """

        info("Poll solar woke up")
        solar = envoy.get_current_data()
        if solar.watthours_lifetime != self._previous_solar:
            self._db.insert_solar(solar.timestamp, solar.watthours_lifetime)
            self._previous_solar = solar.watthours_lifetime


class QueryHandler(object):
    """An interface class to be exported via Pyro that hides module internal
    functionality of HomeDB."""

    def __init__(self, dbase):
        self._dbase = dbase

    @Pyro4.expose
    def debug_dump(self):
        """Dump the database content to the log file. Use with caution."""
        self._dbase.debug_dump()

    @Pyro4.expose
    def get_latest_values(self, count, what):
        """Get latest demand/pge/solar values. See HomeDB documentation."""
        return self._dbase.get_latest_values(count, what)

    @Pyro4.expose
    def get_values_for(self, timelist, what):
        """Get demand/pge/solar values for a list of timestamps. See HomeDB
        documentation."""

        return self._dbase.get_values_for(timelist, what)

    @Pyro4.expose
    def get_interpolated_values_for(self, timelist, what):
        """Get interpolated demand/pge/solar values for a list of timestamps.
        See HomeDB documentation."""

        return self._dbase.get_interpolated_values_for(timelist, what)

    @Pyro4.expose
    def get_range(self, start, end, what):
        """Get a range of demand/pge/solar numbers. See HomeDB documentation."""

        return self._dbase.get_range(start, end, what)

    @Pyro4.expose
    def ping(self):
        """Ping function to let callers test whether we are open for business."""
        pass


def run():
    """Main function. Launches database handler, starts polling of smart meter
    and envoy and exposes the database interface via Pyro."""

    logger, handler = mylogger.get_logger(MYSETTINGS['logger'])
    mylogger.set_logger(logger)

    dbe = HomeDB()
    poller = Poller(dbe, settings['rainforest']['address'],
                    settings['rainforest']['metermacid'], 5, 12)
    poller.run()

    publisher = QueryHandler(dbe)
    if publisher is None:
        raise "DID NOT GET A PUBLISHER"

    info("Publisher {0:}. Starting Pyro service".format(publisher))
    Pyro4.Daemon.serveSimple(
        {
            publisher: MYSETTINGS['service'],
        },
        ns=False,
        host=MYSETTINGS['servicehost'],
        port=MYSETTINGS['serviceport'],
    )


if __name__ == "__main__":
    run()
