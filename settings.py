"""Default settings for various SolarAggregator components."""

settings = {
    # Settings for the main database server.
   'homedb':{'logger': '/var/solar/logs/homedb.out',
             'service': 'homedb.service',
             'servicehost': 'localhost',
             'serviceport': 51300,
             'dbname': '/var/solar/home.db',
   },
   
   # Settings for querying the Envoy (Enphase)
   'envoy':{'serialno':'121424011187',
            'address':'192.168.1.157',
   },
   
   # Settings for the Bidgely data uploader.
   'uploader':{'logger':'/var/solar/logs/uploader.out',
               'bidgely_key':'0aed8eb8-faa1-489f-b803-881e6c878a51',
   },
   
   # Settings for the REST API Server
   'sdata':{'logger': '/var/solar/apilogs/sdata.out',
   },
   
   # Settings for the Zigbee Rainforest Eagle to read your
   # electric meter.
   'rainforest': { 
      # The IP address of your Zigbee rainforest device
      'address':'192.168.1.110',
      # The MAC ID of your meter that you want your Zigbee to read.
      'metermacid':'d8d5b90000003e00',
   },
}
