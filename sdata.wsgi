#!/usr/bin/python
from flask import Flask, request
import flask
import Pyro4
import dateutil.tz
import pytz
import tzlocal
import sys
sys.path.append('/opt/solar')
import utils
import mylogger
from mylogger import info, warn
from settings import settings
from datetime import datetime, timedelta

app = Flask(__name__)
app.secret_key = 'foobar12345'

counter = 0
poller = None

MAX_WATTS = 3000

def datetime_to_unix(dto):
  """Convert a datetime object to a unix timestamp."""

  timestamp = (dto - datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds()
  return int(timestamp)


def unix_to_datetime(timestamp):
  dto = datetime.fromtimestamp(timestamp, pytz.utc)
  return dto.astimezone(dateutil.tz.tzlocal())


def datetime_to_yearstring(dto):
  return "{0:}/{1:}/{2:}".format(dto.month, dto.day, dto.year)


def datetime_to_timestring(dto):
  hour = (dto.hour % 12)
  if hour == 0:
    hour = 12
  minute = dto.minute
  if dto.hour >= 12:
    ampm = "PM"
  else:
    ampm = "AM"
  return "{0:}:{1:02}{2:}".format(hour, minute, ampm)


def YearStringToDatetime(yearStr):
  year,month,day= [int(x) for x in yearStr.split("-")]
  dto = datetime(year, month, day, tzinfo=dateutil.tz.tzlocal());
  return dto


def start_of_this_month():
    """Get a datetime object representing the start of the current
    month in the local timezone."""

    dto = start_of_today()
    return dto.replace(day=1)


def start_of_today():
    """Get a datetime object representing the start of today in the
    local timezone."""

    dto = datetime.now(tzlocal.get_localzone())
    return dto.replace(hour=0, minute=0, second=0, microsecond=0)


def start_of_this_month():
    """Get a datetime object representing the start of the current
    month in the local timezone."""

    dto = start_of_today()
    return dto.replace(day=1)


def convert_daily_result(stats):
    result = []
    counter = 1

    time_list = [x for x in sorted(stats.keys())]
    for time_idx in range(len(time_list) - 1):
      solar = stats[time_list[time_idx+1]]['solar'][0] - \
              stats[time_list[time_idx]]['solar'][0]

      pge_in = stats[time_list[time_idx+1]]['pge'][0] - \
              stats[time_list[time_idx]]['pge'][0]

      pge_out = stats[time_list[time_idx+1]]['pge'][1] - \
              stats[time_list[time_idx]]['pge'][1]
      pge = pge_in - pge_out

      timestamp = time_list[time_idx]

      result.append([timestamp, float(pge)/1000, float(solar)/1000])
    return result


def convert_to_watts(data, secs, max_valid):
  for item in data:
    # pge or solar -- first value is a timestamp
    for idx in xrange(1, len(item)):
      watts = int(1000 * item[idx] * (3600/secs))
      if abs(watts) > max_valid:
         # Ignore obviously invalid numbers
         watts = 0
      item[idx] = watts

def get_kwhs(pge_old, pge_new, solar_old, solar_new):
   pge_net = float((pge_new[1] - pge_old[1]) - \
                  (pge_new[2] - pge_old[2]))
   solar_net = float(solar_new[1] - solar_old[1])
   return pge_net/1000, solar_net/1000

def get_kwatts(pge_old, pge_new, solar_old, solar_new):
   pge_net, solar_net = get_kwhs(pge_old, pge_new, solar_old, solar_new)

   time_delta = pge_new[0] - pge_old[0]
   pge_net *= (3600/time_delta)

   time_delta = solar_new[0] - solar_old[0]
   solar_net *= (3600/time_delta)

   return pge_net, solar_net

def datetimes_to_yearstrings(dtolist):
  result = []

  for entry in dtolist:
    converted = [datetime_to_yearstring(unix_to_datetime(entry[0]))]
    converted.extend(entry[1:])
    result.append(converted)
  return result


def datetimes_to_timestrings(dtolist):
  for entry in dtolist:
    entry[0] = datetime_to_timestring(unix_to_datetime(entry[0]))


def make_timelist(start, end, delta):
  datelist = []

  while start <= end:
    datelist.append(datetime_to_unix(start))
    start += delta
  return datelist

@app.route("/daily", methods=['GET'])
def getDailyData():
  start = YearStringToDatetime(request.args.get('start', 'NONE'))
  end = YearStringToDatetime(request.args.get('end', 'NONE'))
  one_day = timedelta(days = 1)
  end = end + one_day
  datelist = make_timelist(start, end, one_day)

  stats = poller.get_interpolated_values_for(datelist, ['pge', 'solar'])
  result_raw = convert_daily_result(stats)
  result = datetimes_to_yearstrings(result_raw)
  #info("/daily SENDING RESULT: {0:}".format(result))
  return flask.jsonify({'daily':result})


@app.route("/daywatts", methods=['GET'])
def getDayWatts():
  secs = 300

  day = YearStringToDatetime(request.args.get('day', 'NONE'))
  info(">>> DAY REQUEST: {0:}".format(day))
  end = day + timedelta(days=1)
  now = datetime.now(tzlocal.get_localzone())
  if end > now:
     # Don't attempt to return data from the future...
     end = now
  delta = timedelta(seconds=secs)
  datelist = make_timelist(day, end, delta)

  stats = poller.get_interpolated_values_for(datelist, ['pge', 'solar'])
  result = convert_daily_result(stats)
  convert_to_watts(result, secs, MAX_WATTS)
  datetimes_to_timestrings(result)
  #info("/daywatts SENDING RESULT: {0:}".format(result))
  return flask.jsonify({'intervals': result})

def netPGE(data, index):
  return data['pge'][index][1] - data['pge'][index][2]

@app.route("/current", methods=['GET'])
def getCurrent():
  dataNow = poller.get_latest_values(2, ['pge', 'solar', 'demand'])

  today = datetime_to_unix(start_of_today())
  dataDay = poller.get_values_for([today], ['pge', 'solar'])

#  return flask.jsonify({"data": dataDay})

  month = datetime_to_unix(start_of_this_month())
  dataMonth = poller.get_values_for([month], ['pge', 'solar'])

  # pge net is total_in - total_out
  pge_now = netPGE(dataNow, 0)
  pge_day = netPGE(dataDay, 0)
  pge_month = netPGE(dataMonth, 0)

  solar_now = dataNow['solar'][0][1]
  solar_day = dataDay['solar'][0][1]
  solar_month = dataMonth['solar'][0][1]

  pge_day = pge_now - pge_day
  house_day = (solar_now - solar_day) + pge_day
  if pge_day < 0:
     house_day_solar = (solar_now - solar_day) + pge_day
  else:
     house_day_solar = (solar_now - solar_day)

  pge_month = pge_now - pge_month
  house_month = (solar_now - solar_month) + pge_month
  if pge_month < 0:
     house_month_solar = (solar_now - solar_month) + pge_month
  else:
     house_month_solar = solar_now - solar_month

  data = { 'current': dataNow['demand'][0][1] }

  data['day'] = { 'pge': pge_day,
                  'solar': solar_now - solar_day,
                  'house': house_day,
                  'house-solar': house_day_solar,
  }

  data['month'] = { 'pge': pge_month,
                    'solar': solar_now - solar_month,
                    'house': house_month,
                    'house-solar': house_month_solar,
  }

  pge_net, solar_net = get_kwatts(dataNow['pge'][1], dataNow['pge'][0],
                                  dataNow['solar'][1], dataNow['solar'][0])

  if pge_net < 0:
     # We are exporting. -- Subtract the PGE export from solar...
     solar_net += pge_net

  if solar_net < 0:
     solar_net_house = solar_net + pge_net
  else:
     solar_net_house = solar_net

  data['latest'] = { 'pge': int(pge_net * 1000),
                     'solar': int(solar_net * 1000),
                     'house-solar': int(solar_net_house * 1000)
  }

  return flask.jsonify({'data': data })


def Setup():
    global poller

    logFile = settings['sdata']['logger']

    logger, handler = mylogger.get_logger(logFile)
    mylogger.set_logger(logger)

    service_name = "PYRO:{0}@{1}:{2}".format(settings['homedb']['service'],
                                             settings['homedb']['servicehost'],
                                             settings['homedb']['serviceport'])

    poller = Pyro4.Proxy(service_name)



Setup()
application = app

if __name__ == "__main__":
  app.run(host='0.0.0.0')
