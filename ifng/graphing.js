var dgraph = {
    firstcall: true,
    xAxis: 0,
    yAxis: 0,

    key: function(data) {
	return data[0];
    },

    MakeDateString: function(dateObj) {
	return dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1) +
	    '-' + dateObj.getDate();
    },

    ShowTooltip: function(label, myBar, hOffset, d) {
	var xPosition = Math.round(parseFloat(myBar.attr("x")) +
			    parseFloat(myBar.attr("width")) / 2);
	var yPosition = hOffset;

	//Update the tooltip position and value
	ttip = d3.select(label)
	    .style("left", xPosition + "px")
	    .style("top", yPosition + "px");

	ttip.select("#heading").text(d[0]);
	ttip.select("#solar").text(d[2].toFixed(2));
	ttip.select("#house").text((d[1]+d[2]).toFixed(2));
	ttip.select("#pge").text(d[1].toFixed(2));
	//Show the tooltip
	d3.select(label).classed("hidden", false);
    },


    MakeDayTicks: function() {
	var tl = [];
	var ampm = ["AM", "PM"];
	var hours= ["12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];

	for (half = 0; half < ampm.length; half++) {
	    tag = ampm[half];

	    for (hour = 0; hour < hours.length; hour++) {
		tl.push(hours[hour]+":00"+tag);
	    }
	}
	return tl;
    },


    MakeGraph: function(data, label_name, barClickFunc, tickMarks, headerText) {
	//console.log("MAKE GRAPH: HEADER TEXT: " + headerText);

	var yPadding = 60;
	var xPadding = 30
	var yPadTop = 5;
	var svg = d3.selectAll(label_name + "-graph");
	var w = 800;
	var h = 450;
	var yScale = d3.scale.linear();
	var xScale = d3.scale.ordinal();
	var manyVals;
	var yMin;
	var yMax;

	/*
	var svgBox = svg.node().getBBox();
	w = svgBox.width;
	h = svgBox.height;
	console.log("MAKE GRAPH SVG: " + w + "/" + h + "/" + svgBox);
	*/

	manyVals = (data.length > 40);

	if (headerText == null) {
	    d3.select(label_name + "-header")
		.text('From ' + data[0][0] + ' To ' + data[data.length-1][0]);
	} else {
	    d3.select(label_name + "-header")
		.text(headerText);
	}

	yMin = d3.min(data, function(d) { return d[1]; });
	if (yMin > 0) {
	    yMin = 0;
	}
	yMax = d3.max(data, function(d) { return d[1] + d[2]; });

	yMinLen = ("" + yMin.toFixed()).length;
	yMaxLen = ("" + yMax.toFixed()).length;
	if (yMinLen > yMaxLen) {
	    yMaxLen = yMinLen;
	}

	xPadding = 8 * yMaxLen + 10;

	if (manyVals) {
	    xScale.rangeBands([xPadding, w], 0);
	} else {
	    xScale.rangeRoundBands([xPadding, w], .1);
	}

	xScale.domain(data.map(dgraph.key));

	yScale.domain([yMin, yMax]).nice();
	yScale.range([h - yPadding, yPadTop])

	// Solar consumed bar
	sol = svg.selectAll(".solar-bar")
	    .data(data, dgraph.key);
	sol.enter()
	    .append("rect")
            .attr("class", "solar-bar")
            .attr("x", function(d) { return xScale(dgraph.key(d)); })
            .attr("y", function(d) {
		return yScale(d[1] + d[2]);
            })
            .attr("width", xScale.rangeBand())
            .attr("height", function(d) {
		if (d[1] > 0) { // PG&E Import
		    return yScale(d[1]) - yScale(d[1] + d[2]);
		}
		return yScale(0) - yScale(d[1]+d[2]);
            })
	    .on("mouseover", function(d) {
		//Get this bar's x/y values, then augment for the tooltip
		dgraph.ShowTooltip(label_name + "-tooltip", d3.select(this), h / 2, d);
	    })
	    .on("mouseout", function() {
		//Hide the tooltip
		d3.select(label_name + "-tooltip").classed("hidden", true);
	    })
	    .on("click", barClickFunc);

	sol.transition()
            .attr("x", function(d) { return xScale(dgraph.key(d)); })
            .attr("y", function(d) {
		return yScale(d[1] + d[2]);
            })
            .attr("width", xScale.rangeBand())
            .attr("height", function(d) {
		if (d[1] > 0) { // PG&E Import
		    return yScale(d[1]) - yScale(d[1] + d[2]);
		}
		return yScale(0) - yScale(d[1]+d[2]);
            });

	sol.exit()
	    .transition()
	    .attr("x", -xScale.rangeBand())
	    .remove();

	// PG&E bar
	pge = svg.selectAll(".pge-bar").data(data, dgraph.key);

	pge.enter()
	    .append("rect")
            .attr("class", "pge-bar")
            .attr("x", function(d) { return xScale(dgraph.key(d)); })
            .attr("y", function(d) {
		if (d[1] > 0) { // PG&E Import
                    return yScale(d[1]);
		}
		return yScale(0);
            })
            .attr("width", xScale.rangeBand())
            .attr("height", function(d) {
		if (d[1] > 0) { // PG&E Import
		    return yScale(0) - yScale(d[1]);
		}
		return yScale(d[1]) - yScale(0);
            })
            .attr("fill", function(d) {
		if (d[1] > 0) { // PG&E Import
                    return "steelblue";
		}
		return "seagreen";
            })
	    .on("mouseover", function(d) {
		//Get this bar's x/y values, then augment for the tooltip
		dgraph.ShowTooltip(label_name + "-tooltip", d3.select(this), h / 2, d);
	    })
	    .on("mouseout", function() {
		//Hide the tooltip
		d3.select(label_name + "-tooltip").classed("hidden", true);
	    })
	    .on("click", barClickFunc);


	pge.transition()
            .attr("x", function(d) { return xScale(dgraph.key(d)); })
            .attr("y", function(d) {
		if (d[1] > 0) { // PG&E Import
                    return yScale(d[1]);
		}
		return yScale(0);
            })
            .attr("width", xScale.rangeBand())
            .attr("height", function(d) {
		if (d[1] > 0) { // PG&E Import
		    return yScale(0) - yScale(d[1]);
		}
		return yScale(d[1]) - yScale(0);
            })
            .attr("fill", function(d) {
		if (d[1] > 0) { // PG&E Import
                    return "steelblue";
		}
		return "seagreen";
            });

	pge.exit()
	    .transition()
	    .attr("x", -xScale.rangeBand())
	    .remove();

	svg.selectAll("g.axis").remove();

	dgraph.xAxis = d3.svg.axis();
	dgraph.xAxis.scale(xScale)
	    .orient("bottom");

	if (tickMarks != null) {
	    dgraph.xAxis.tickValues(tickMarks);
	}

	svg.append("g")
	    .call(dgraph.xAxis)
	    .attr("class", "axis xaxis")
	    .attr("transform", "translate(0," + (h - yPadding ) + ")")
	    .selectAll("text")
	    .style("text-anchor", "end")
	    .attr("dx", "-.8em")
	    .attr("dy", ".15em")
	    .attr("transform", function(d) {
                return "rotate(-65)"
	    });


	dgraph.yAxis = d3.svg.axis();
	dgraph.yAxis.scale(yScale)
	    .orient("left");

	svg.append("g")
	    .call(dgraph.yAxis)
	    .attr("class", "axis yaxis")
	    .attr("transform", "translate(" + xPadding + ",0)");

    },


    dayTabTemplate: "<li><a href='#{href}'>#{label}</a>" +
	"<span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",


    MakeDayTab: function(label) {
	var content =
	    "<div id='day-"+label+"'>" +
	    "<h2><span id='day-"+label+"-header'>Please wait...</span></h2>" +
	    "<div class='svg-container'>" +
	    "<svg id='day-"+label+"-graph', viewBox='0 0 800 450' " +
	          "preserveAspectRatio='xMinYMin meet' class='svg-content'>" +
	    "</div>" +

	"<div id='day-"+label+"-tooltip' class='hidden tooltip'>" +
	    "<p><strong>Time: <span id='heading'>X</span></strong></p>" +
	    "<p>Solar: <span id='solar'>100</span></p>" +
	    "<p>House: <span id='house'>100</span></p>" +
	    "<p>PG&amp;E: <span id='pge'>100</span></p>" +
	    "</div>" +
	    "</div>";
	console.log("TAB: " + content);
	return content;
    },


    indexForTab: function(tabId) {
	return $("#tabs a[href='"+tabId+"']").parent().index();
    },


    ZoomIntoDay: function(data) {
	console.log(data);

	var tabId = data[0].replace(/\//g, "-");
	var tabLabel = data[0];
	var li = $( dgraph.dayTabTemplate.replace( /#\{href\}/g, "#day-" + tabId )
		    .replace( /#\{label\}/g, tabLabel ) );

	// Before we do anything, let's see if we already have that tab open...
	var index = dgraph.indexForTab('#day-' + tabId);

	if (index == -1) {
	    // We don't have this tab open yet. -- Open it...

	    tabs.find("ul").append(li);

	    console.log("LI: " +tabId);

	    tabContent = dgraph.MakeDayTab(tabId);
	    tabs.append(tabContent);
	    tabs.tabs("refresh");

	    // Re-query the tab index. We must have that tab now...
	    index = dgraph.indexForTab('#day-' + tabId);

	    $("#tabs").tabs({active: index});
	    dayFactory = new DayCallback(data[0], tabId);
	    dayFactory.getData();
	} else {
	    $("#tabs").tabs({active: index});
	}
    },
}



function DayCallback(date, label) {
    this.date = date;
    this.label = label;
    this.timer = null;

    this.showsToday = function()
    {
	var today = new Date();
	var dateComponents = this.label.split("-");
	var showsToday;

	year = parseInt(dateComponents[2]);
	month = parseInt(dateComponents[0]);
	day = parseInt(dateComponents[1]);

	showsToday = (year == today.getFullYear()) && (month - 1 == today.getMonth()) &&
            (day == today.getDate());
	return showsToday;
    },


    this.getData = function()
    {
	var dateComponents = this.label.split("-");
	var showsToday = false;
	var year, month, day;
        var today = this.showsToday();

	dateArg = dateComponents[2]+"-"+dateComponents[0]+"-"+dateComponents[1];

	dayLabel = "#day-" + this.label;
	header = this.date;
	if (today) {
	    header += " (Today)";
	}

	console.log("XXX THIS LABEL GETDATA: " + header);

	$.get(dayWattsUrl, {day: dateArg }, function(data) {
            dgraph.MakeGraph(data.intervals, dayLabel,
                             function(){}, dgraph.MakeDayTicks(), header);
	});

	if (today) {
	    var thisObject = this;

            this.timer = setTimeout(function() {thisObject.getData()}, 60000);
            $('#day-' + this.label).data("DayCallback", this);
	}
    },

    this.cancelTimer = function()
    {
	if (this.timer != null) {
            clearTimeout(this.timer);
            this.timer = null;
	}

    }
};


var currentGraph = {
    width: 800,
    height: 450,


    MakeGraph: function(label_name)
    {
	var svg = d3.selectAll("#" + label_name + "-graph");

	var width = currentGraph.width;
	var height = currentGraph.height;

	svg.append("text")
	    .attr("x", width/2)
	    .attr("y", height/2)
	    .attr("text-anchor", "middle")
	    .text("House / PGE / Solar")
	    .attr("fill", "black");

	dataFactory = new CurrentCallback(label_name);
	dataFactory.getData();
    },

    DrawGraph: function(data, label_name)
    {
	var svg = d3.selectAll("#" + label_name + "-graph");

	var width = currentGraph.width;
	var height = currentGraph.height;

	txtNow = svg.selectAll(".txtNow")
	    .data([data['data']['current']]);
	txtNow.enter()
	    .append("text")
	    .attr("class", "txtNow")
	    .attr("x", width/2)
	    .attr("y", height/2 - 15)
	    .attr("text-anchor", "middle")
	    .text(function(d) { return "Now: " + currentGraph.KWText(d, 4, false, true); })
	    .attr("fill", function(d) {
		if (d <= 0) {
		    return "green";
	        } else {
		    return "red";
		}
	    });
	txtNow.transition()
	    .text(function(d) { return "Now: " + currentGraph.KWText(d, 4, false, true); })
	    .attr("fill", function(d) {
		if (d <= 0) {
		    return "green";
	        } else {
		    return "red";
		}
	    });

	dayVals = svg.selectAll(".dayVals")
	    .data([data['data']['day']]);
	dayVals.enter()
	    .append("text")
	    .attr("class", "dayVals")
	    .attr("x", width/2)
	    .attr("y", height/2 + 15)
	    .attr("text-anchor", "middle")
	    .text(function(d) {
		return "Day: " + currentGraph.KWText(d['house'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['pge'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['solar'], 3, true, false);
	    })
	    .attr("fill", function(d) {
		if (d['pge'] <= 0) {
		    return "green";
	        } else {
		    return "red";
		}
	    });

	dayVals.transition()
	    .text(function(d) {
		return "Day: " + currentGraph.KWText(d['house'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['pge'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['solar'], 3, true, false);
	    })
	    .attr("fill", function(d) {
		if (d['pge'] <= 0) {
		    return "green";
	        } else {
		    return "red";
		}
	    });

	monthVals = svg.selectAll(".monthVals")
	    .data([data['data']['month']]);
	monthVals.enter()
	    .append("text")
	    .attr("class", "monthVals")
	    .attr("x", width/2)
	    .attr("y", height/2 + 30)
	    .attr("text-anchor", "middle")
	    .text(function(d) {
		return "Month: " + currentGraph.KWText(d['house'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['pge'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['solar'], 3, true, false);
	    })
	    .attr("fill", function(d) {
		if (d['pge'] <= 0) {
		    return "green";
	        } else {
		    return "red";
		}
	    });
	monthVals.transition()
	    .text(function(d) {
		return "Month: " + currentGraph.KWText(d['house'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['pge'], 3, true, false) + "/ " +
		    currentGraph.KWText(d['solar'], 3, true, false);
	    })
	    .attr("fill", function(d) {
		if (d['pge'] <= 0) {
		    return "seagreen";
	        } else {
		    return "red";
		}
	    });

	currentGraph.DrawArc(svg, "currentArc", 140, [-2000, 2000], data['data']['latest']);
	currentGraph.DrawArc(svg, "dayArc", 170, [-20000, 20000], data['data']['day']);
	currentGraph.DrawArc(svg, "monthArc", 200, [-600000, 600000], data['data']['month']);
    },


    DrawArc: function(svg, label, innerRadius, domain, data)
    {
	var width = currentGraph.width;
	var height = currentGraph.height;

	var PI = 3.1415926;
	var myScale = d3.scale.linear()
	    .domain(domain)
	    .range([-PI-PI/2, PI-PI/2]);

	var arc = d3.svg.arc()
	    .innerRadius(innerRadius)
	    .outerRadius(innerRadius + 20)
	    .startAngle(function(d) { return myScale(d[0]); })
	    .endAngle(function(d) { return myScale(d[1]); });

	var dayd = [];

	if (data['pge'] < 0) {
	    // We are EXPORTING
	    dayd.push([data['pge'], 0, 'lightgreen'], [0, data['house-solar'], 'yellow']);
	} else {
	    // We are IMPORTING
	    dayd.push([0, data['pge'], 'steelblue'],
		      [data['pge'], data['pge'] + data['house-solar'], 'yellow']);
	}

	dayArc = svg.selectAll("." + label)
	    .data(dayd);

	dayArc.enter()
	    .append("path")
	    .attr("class", label)
	    .attr("d", arc)
	    .attr("transform", "translate(" + width/2 +"," + height/2 + ")")
	    .attr("fill", function(d) { return d[2]; })
	    .attr("stroke", "black")
	    .attr("stroke-width", .5);

	dayArc.transition()
	    .attr("d", arc)
	    .attr("fill", function(d) { return d[2]; });

    },


    KWText: function(wattsTxt, precision, forceKW, addUnit)
    {
	watts = parseInt(wattsTxt);
	if (Math.abs(watts) > 1000 || forceKW) {
	    watts = watts / 1000;
	    unit = " kW";
	    wattsTxt = watts.toPrecision(precision);
	} else {
	    unit = " W";
	}
	if (addUnit) {
	    wattsTxt = wattsTxt + unit;
	}
	return wattsTxt;
    },


    GetData: function()
    {
	var thisObject = this;

        this.timer = setTimeout(function() {thisObject.getData()}, 60000);
        $('#day-' + this.label).data("DayCallback", this);
    },

}; // currentGraph


function CurrentCallback(label)
{
    this.label = label;

    this.getData = function()
    {
	$.get(currentUrl, {}, function(data) {
	    currentGraph.DrawGraph(data, label);
	});

	var thisObject = this;
	this.timer = setTimeout(function() {thisObject.getData();}, 20000);
	$('#' + this.label).data("CurrentCallback", this);
    };

    this.cancelTimer = function()
    {
	if (this.timer != null) {
	    clearTimeout(this.timer);
	    this.timer = null;
	}
    };
};



var todayGraph = {
    parseDate: 0,
    x: 0,
    y: 0,
    width: 800,
    height: 450,
    yPadding: 50,
    xPadding: 50,

    MakeGraph: function(label)
    {
	var svg = d3.selectAll("#" + label + "-graph");
	var xAxis = d3.svg.axis();

	todayGraph.parseDate = d3.time.format("%-I:%M%p").parse;
	parseDate = todayGraph.parseDate;

	todayGraph.x = d3.time.scale()
	    .range([todayGraph.xPadding, todayGraph.width - todayGraph.xPadding])
	    .domain([parseDate("12:00AM"), parseDate("11:55PM")]);
	todayGraph.y = d3.scale.linear()
	    .range([todayGraph.height - todayGraph.yPadding, 0]);

	dataFactory = new TodayCallback(label);
	dataFactory.getData();

	xAxis.scale(todayGraph.x)
	    .orient("bottom")
	    .tickFormat(d3.time.format("%-I%p"));

	svg.append("g")
	    .call(xAxis)
	    .attr("transform", "translate(0, " +
		  (todayGraph.height - todayGraph.yPadding ) + ")")
	    .attr("class", "axis xaxis")
	    .selectAll("text")
	    .style("text-anchor", "end")
	    .attr("dx", "-.8em")
	    .attr("dy", ".15em")
	    .attr("transform", function(d) {
                return "rotate(-65)"
	    });


	svg.append("path").attr("class", label)
	    .attr("fill", "none")
	    .attr("stroke", "seagreen")
	    .attr("stroke-width", .5);
    },

    DrawGraph: function(data, label)
    {
	var parseDate = todayGraph.parseDate;
	var svg = d3.selectAll("#" + label + "-graph");
	var x = todayGraph.x;
	var y = todayGraph.y;

	//console.log("Drawing TodayGraph");

	y.domain([0, d3.max(data['intervals'], function(d) { return d[2]; })]);

	var yAxis = d3.svg.axis();

	yAxis.scale(todayGraph.y)
	    .orient("left");

	svg.selectAll("g.yaxis").remove();

	svg.append("g")
	    .call(yAxis)
	    .attr("class", "axis yaxis")
	    .attr("transform", "translate("+todayGraph.xPadding+", 0)");

	var line = d3.svg.line()
	    .x(function(d) { return x(parseDate(d[0])); })
	    .y(function(d) { return y(d[2]); });

	todayG = svg.selectAll("." + label)
	    .datum(data['intervals']);

	todayG.transition()
	    .attr("d", line);

    },
};


function TodayCallback(label)
{
    this.label = label;

    this.getData = function()
    {
	var today = new Date();

	todayStr = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

	$.get(dayWattsUrl, { day: todayStr }, function(data) {
	    todayGraph.DrawGraph(data, label);
	});

	var thisObject = this;
	this.timer = setTimeout(function() {thisObject.getData();}, 150000);
	$('#' + this.label).data("TodayCallback", this);
    };

    this.cancelTimer = function()
    {
	if (this.timer != null) {
	    clearTimeout(this.timer);
	    this.timer = null;
	}
    };
};
