"""Simple logging module.
   (C) 2015, Christian Czezatke"""

import logging
import logging.handlers

GLOGGER = None

def set_logger(logger):
    """Set the global logging object."""

    global GLOGGER
    GLOGGER = logger

def get_logger(filename, loggername=None):
    """Get a Logger and a log handler object."""
    handler = logging.handlers.RotatingFileHandler(filename,
                                                   maxBytes=1024 * 1024,
                                                   backupCount=3)
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    handler.setFormatter(formatter)

    logger = logging.getLogger(loggername)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger, handler

def info(txt):
    """Log an info message to the default logger."""
    GLOGGER.info(txt)

def error(txt):
    """Log an error message to the default logger."""
    GLOGGER.error(txt)

def warn(txt):
    """Log a warning message to the default logger."""
    GLOGGER.warn(txt)
