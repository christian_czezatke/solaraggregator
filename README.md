![current.png](https://bitbucket.org/repo/69Rk56/images/2156690615-current.png)
![today.png](https://bitbucket.org/repo/69Rk56/images/2649914493-today.png)
![d3-if1.png](https://bitbucket.org/repo/69Rk56/images/1498588674-d3-if1.png)
![d3-if2.png](https://bitbucket.org/repo/69Rk56/images/2048947821-d3-if2.png)
## Description ##

A suite of Python scripts to aggregate electric usage data from a
smart meter and a rooftop solar installation. Also, it is possible to
upload the data to the Bidgely Home Energy Monitoring service, which
should give you more accurate data since solar generation no longer
has to be estimated.

Currently, this only works with a Rainforest Eagle Zigbee gateway that
is reading a PG&E smart meter, and an Enphase Envoy communication
gateway.

Since with net metering it is otherwise difficult to keep track of
actual consumption, these scripts gather data, store it in a sqlite
database and aggregate it. The current user visible output is a set of
pretty graphs that can be viewed in a browser and update in real time.

## Installation ##

Here are the steps to install this software on a Raspberry Pi:

* From the directory that contains this file, as root, run the following
  command:
  ./install.sh
* Tell apache to enable the REST API for this package:
  * Edit the file /etc/apache2/sites-available/default
  * Add the following section inside the "VirtualHost" block:

```
#!xml

         WSGIScriptAlias /api /var/www/api
        <Directory /var/www/api>
                AddHandler wsgi-script .wsgi
                Order allow,deny
                Allow from all
        </Directory>
```

* Restart apache: service apache2 restart
* Start the solar monitoring service: service solar start

From now onwards, whenever your PI reboots, the solar monitor will be restarted.

## Links ##
* [Rainforest Eagle Communication Gateway](http://rainforestautomation.com/rfa-z109-eagle/)
* [Enphase Envoy](http://enphase.com/envoy/)